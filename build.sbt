name := """atv-moc"""
organization := "gov.tubitak.eu"

version := "1"

scalaVersion := "2.12.4"

lazy val atvMoc = (project in file(".")).enablePlugins(PlayScala)

resolvers += "Eid public repository" at "http://mindertestbed.org:8081/nexus/content/groups/public/"

resolvers += "Binary Plugin Releases" at "https://dl.bintray.com/sbt/sbt-plugin-releases/"

resolvers += Resolver.mavenLocal

resolvers += Resolver.sbtPluginRepo("releases")

libraryDependencies ++= Seq(
  guice,
  "gov.tubitak.minder" % "minder-common" % "1.0.0",
  "gov.tubitak.minder" % "minder-client" % "1.0.1",
  "gov.tubitak.eu" % "lightest-minder-common" % "1.1.4",
  "gov.tubitak.uekae.ma3" % "ma3api-signature" % "2.1.2",
  "gov.tubitak.uekae.ma3" % "ma3api-asn" % "2.1.2",
  "gov.tubitak.uekae.ma3" % "ma3api-common" % "2.1.2",
  "gov.tubitak.uekae.ma3" % "ma3api-crypto" % "2.1.2",
  "gov.tubitak.uekae.ma3" % "ma3api-asn1" % "6.6.4",
  "gov.tubitak.uekae.ma3" % "ma3api-asic" % "2.1.2",
  "gov.tubitak.uekae.ma3" % "ma3api-xmlsignature" % "2.1.2",
  "gov.tubitak.uekae.ma3" % "ma3api-certvalidation" % "2.1.2",
  "gov.tubitak.uekae.ma3" % "ma3api-crypto-gnuprovider" % "2.1.2",
  "gov.tubitak.uekae.ma3" % "ma3api-certstore" % "2.1.2",
  "gov.tubitak.uekae.ma3" % "ma3api-sqllite" % "3.7.2",
  "com.adrianhurt" %% "play-bootstrap" % "1.2-P26-B4",
  "org.webjars.bower" % "bootstrap-sass" % "3.3.6",
  //"org.webjars" % "bootstrap-sass" % "3.3.7" exclude("org.webjars", "jquery"),
  "org.webjars" % "jquery" % "3.3.1",
  "org.webjars" % "font-awesome" % "4.7.0",
  "org.webjars.bower" % "compass-mixins" % "0.12.7",
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test
)