resolvers ++= Seq(
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
  "Typesafe Releases" at "http://repo.typesafe.com/typesafe/maven-releases/",
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
  "Eid public repository" at "http://mindertestbed.org:9503/nexus/content/groups/public/",
  "Bintray Plugin Releases" at "https://dl.bintray.com/sbt/sbt-plugin-releases/",
  Resolver.mavenLocal,
  Resolver.sbtPluginRepo("releases")
)


addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.11")

addSbtPlugin("org.irundaia.sbt" % "sbt-sassify" % "1.4.11")
