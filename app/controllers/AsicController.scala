package controllers

import javax.inject.Inject

import models.Asic
import play.api.data._
import play.api.mvc._
import utils.{AsicUtils, DNSClientUtils}

/**
  * The classic WidgetController using MessagesAbstractController.
  *
  * Instead of MessagesAbstractController, you can use the I18nSupport trait,
  * which provides implicits that create a Messages instance from a request
  * using implicit conversion.
  *
  * See https://www.playframework.com/documentation/2.6.x/ScalaForms#passing-messagesprovider-to-form-helpers
  * for details.
  */
class AsicController @Inject()(cc: MessagesControllerComponents) extends MessagesAbstractController(cc) {

  import AsicForm._

  private lazy val asicUtils: AsicUtils = new AsicUtils();
  private val dnsUtils = new DNSClientUtils();

  private val SIGNER_1 = "UMAY";
  private val SIGNER_2 = "URAZ";

  lazy val widgets = scala.collection.mutable.ArrayBuffer(

    // displays the domain names that is extracted from issuer alternate name obtained
    // from ASIC signature signing certificate

    new Asic(SIGNER_1, asicUtils.getIssuerAltName(SIGNER_1)),
    new Asic(SIGNER_2, asicUtils.getIssuerAltName(SIGNER_2)),

  )

  // The URL to the widget.  You can call this directly from the template, but it
  // can be more convenient to leave the template completely stateless i.e. all
  // of the "WidgetController" references are inside the .scala file.
  private val postUrl = routes.AsicController.createWidget()

  def index = Action {
    Ok(views.html.index())
  }

  def listWidgets = Action { implicit request: MessagesRequest[AnyContent] =>
    // Pass an unpopulated form to the template
    Ok(views.html.listAsicContent(widgets, form, postUrl))
  }

  // This will be the action that handles our form post
  def createWidget = Action { implicit request: MessagesRequest[AnyContent] =>

    val errorFunction = { formWithErrors: Form[Data] =>
      // This is the bad case, where the form had validation errors.
      // Let's show the user the form again, with the errors highlighted.
      // Note how we pass the form with errors to the template.
      BadRequest(views.html.listAsicContent(widgets, formWithErrors, postUrl))
    }

    val successFunction = { data: Data =>
      // This is the good case, where the form was successfully parsed as a Data object.

      var deger = "";
      for (e <- widgets) {

        if (e.name == data.name) {

          val issuerDomainName = e.value

          val xmlURL = dnsUtils.issueDNSQuery(asicUtils.getDomainName(issuerDomainName))

          System.out.println("URL " + xmlURL)

          var cert = asicUtils.parseTrustList(xmlURL)

          val result = asicUtils.verifyIssuerCertificate(cert, data.name)

          System.out.println("Verification Result " + result)

          if (result) {
            deger = "For " + data.name + " DNS Query is issued and trust scheme validation is done!!!!"
          }
          else {
            deger = " and the trust scheme validation is FALSE!!"
          }

        }
      }

      Redirect(routes.AsicController.listWidgets()).flashing("info" -> deger)
    }

    val formValidationResult = form.bindFromRequest
    formValidationResult.fold(errorFunction, successFunction)
  }

}
