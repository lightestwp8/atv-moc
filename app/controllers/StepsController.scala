package controllers

import java.io.{ByteArrayInputStream, DataInputStream}
import java.net.URL
import java.util
import javax.inject.{Inject, Singleton}

import gov.tubitak.eu.http.HttpClient
import models.Asic
import play.api.Logger
import play.api.mvc._
import tr.gov.tubitak.uekae.esya.api.asic.core.impl.ASiCSXAdESPackage
import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate
import utils.{AsicUtils, DNSClientUtils}

/**
  * The classic WidgetController using MessagesAbstractController.
  *
  * Instead of MessagesAbstractController, you can use the I18nSupport trait,
  * which provides implicits that create a Messages instance from a request
  * using implicit conversion.
  *
  * See https://www.playframework.com/documentation/2.6.x/ScalaForms#passing-messagesprovider-to-form-helpers
  * for details.
  */
@Singleton
class StepsController @Inject()(cc: MessagesControllerComponents) extends MessagesAbstractController(cc) {
  val LOGGER = Logger(classOf[StepsController])

  var transaction: ASiCSXAdESPackage = null

  private var contentList = new util.ArrayList[Asic]()
  private var sigContentList = new util.ArrayList[Asic]()
  private var issuerCertContentList = new util.ArrayList[Asic]()

  private lazy val asicUtils = new AsicUtils();
  private lazy val dnsUtils = new DNSClientUtils();

  private val SIGNER_1 = "UMAY"
  private val SIGNER_2 = "URAZ"

  private var issuerName = ""
  private var domainFullName = ""
  private var domainName = ""
  private var firstDNSQueryResponse = ""
  private var secondDNSQueryResponse = ""


  var trustListCert: ECertificate = null
  var signerAlias = ""


  def getStep1 = Action { implicit request: MessagesRequest[AnyContent] =>
    LOGGER.debug("STEP1")
    // Pass an unpopulated form to the template
    if (transaction != null) {

      contentList = asicUtils.getAsicFileContent(transaction)
      sigContentList = asicUtils.getSignatureCertificateContent(transaction)
      domainFullName = asicUtils.getIssuerAltName(transaction)
      domainName = asicUtils.getDomainName(domainFullName)
      issuerName = asicUtils.getIssuerName(domainFullName)

      Ok(views.html.step1(contentList, sigContentList, transaction, signerAlias, domainName))

    }
    else
      Ok("Waiting to get an valid electronic transaction")
  }


  def getStep2 = Action { implicit request: MessagesRequest[AnyContent] =>
    LOGGER.debug("STEP3")
    /// issue dns query and render result
    firstDNSQueryResponse = dnsUtils.issueFirstDNSQuery(domainName) //"my dns query response"

    if (!firstDNSQueryResponse.isEmpty) {
      Ok(views.html.step2(contentList, sigContentList, domainName, firstDNSQueryResponse, signerAlias))
    } else {
      Ok("Problem with Step 1, turn to Step 0")
    }


  }

  def getStep3 = Action { implicit request: MessagesRequest[AnyContent] =>
    LOGGER.debug("STEP3")
    /// issue dns query and render result
    secondDNSQueryResponse = dnsUtils.issueSecondDNSQuery(firstDNSQueryResponse) //"my dns query response"

    if (!secondDNSQueryResponse.isEmpty) {
      Ok(views.html.step3(contentList, sigContentList, domainName, firstDNSQueryResponse, secondDNSQueryResponse, signerAlias))
    } else {
      Ok("Problem with Step 2, turn to Step 2")
    }
  }

  def getStep4 = Action { implicit request: MessagesRequest[AnyContent] =>
    LOGGER.debug("STEP4")
    try {
      trustListCert = asicUtils.parseTrustList(secondDNSQueryResponse)
      issuerCertContentList = asicUtils.getCertificateContent(trustListCert)

      if (trustListCert != null && issuerCertContentList != null) {
        Ok(views.html.step4(issuerName, contentList, sigContentList, issuerCertContentList, domainName, firstDNSQueryResponse, secondDNSQueryResponse, signerAlias))
      } else {
        Ok("Problem with Step 3, turn to Step 3")
      }
    } finally {
      val testEnd = new URL("http://localhost:9503/testFinished?success=true&result=muhammet");
      HttpClient.httpGET(testEnd);
    }
  }

  def getStep5 = Action { implicit request: MessagesRequest[AnyContent] =>
    LOGGER.debug("STEP4")
    var verificationResult = asicUtils.verifyIssuerCertificate(trustListCert, transaction)

    if (verificationResult) {
      Ok(views.html.step5(issuerName, contentList, sigContentList, issuerCertContentList, domainName, firstDNSQueryResponse, secondDNSQueryResponse, signerAlias, verificationResult))
    } else {
      Ok("Problem with Step 4, turn to Step 4")
    }
  }

  def manualTrigger(id: String) = Action { implicit request: MessagesRequest[AnyContent] =>
    if (id == "1") {
      transaction = asicUtils.readSignaturePackage(SIGNER_1)
      signerAlias = SIGNER_1
    } else if (id == "2") {
      transaction = asicUtils.readSignaturePackage(SIGNER_2)
      signerAlias = SIGNER_2
    } else {
      Ok("Problem with Electronic Transaction, Please resend the electronic transaction!!")
    }
    Redirect(routes.StepsController.getStep1())
  }

  def reset() = Action {
    transaction = null
    Ok("Done")
  }

  def trigger() = Action(parse.raw) { implicit request: MessagesRequest[RawBuffer] =>
    LOGGER.debug("Received trigger request")
    val bytes = request.body.asBytes().get.toArray

    val dataInputStream = new DataInputStream(new ByteArrayInputStream(bytes))
    val count = dataInputStream.readShort()
    LOGGER.debug(s"Post received $count items: ")

    if (count != 2) {
      BadRequest("You have to send only two items")
    } else {


      //the first item is a transaction,
      var dataLen = dataInputStream.readShort()

      LOGGER.debug(s"Transaction size $dataLen")

      if (dataLen > 1024 * 1024) {
        BadRequest("transaction size too long")
      } else {

        val _transaction = new Array[Byte](dataLen)
        val read = dataInputStream.read(_transaction)
        LOGGER.debug(s"Actual data read $read")

        dataLen = dataInputStream.readShort()

        LOGGER.debug(s"PolicyXml size $dataLen")

        if (dataLen > 1024 * 1024) {
          BadRequest("policy XML size too long")
        } else {


          val policy = new Array[Byte](dataLen)
          dataInputStream.read(policy)

          //policy xml ignored for now

          transaction = asicUtils.readSignaturePackage(_transaction)

          Ok
        }
      }
    }

  }

}
