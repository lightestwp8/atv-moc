package utils;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.security.PrivateKey;
import java.util.List;
import play.api.Play;
import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.common.crypto.BaseSigner;
import tr.gov.tubitak.uekae.esya.api.common.util.bag.Pair;
import tr.gov.tubitak.uekae.esya.api.crypto.alg.SignatureAlg;
import tr.gov.tubitak.uekae.esya.api.crypto.util.PfxParser;
import tr.gov.tubitak.uekae.esya.api.signature.Context;
import tr.gov.tubitak.uekae.esya.api.signature.SignatureFormat;
import tr.gov.tubitak.uekae.esya.api.signature.SignatureType;
import tr.gov.tubitak.uekae.esya.api.signature.config.Config;
import tr.gov.tubitak.uekae.esya.api.signature.sigpackage.PackageType;
import tr.gov.tubitak.uekae.esya.api.signature.sigpackage.SignaturePackage;
import tr.gov.tubitak.uekae.esya.api.signature.sigpackage.SignaturePackageFactory;
import tr.gov.tubitak.uekae.esya.api.signature.util.PfxSigner;

public class AsicBase {

  protected static String ROOT_DIR;           // root directory of project
  protected static String BASE_DIR;           // base directory where signatures created

  protected static File DATA_FILE;            // file to be signed

  protected static ECertificate CERTIFICATE;  // certificate
  protected static BaseSigner SIGNER;         // signer

  protected static boolean IS_QUALIFIED = true;      // gets only qualified certificates in smart card
  protected static String PIN = "123456";                // PIN of the smart card

  protected static String SIGNER1 = "UMAY";
  protected static String SIGNER2 = "URAZ";

  static {

    try {
      ROOT_DIR = Play.current().path().getAbsolutePath();

      System.out.println("ROOT: " + ROOT_DIR);
      BASE_DIR = ROOT_DIR + "/testdata/";
      DATA_FILE = new File(BASE_DIR + "sample.txt");

      /* To sign with pfx file*/
      String PFX_FILE = BASE_DIR + "albania.pfx";
      String PFX_PASS = "123456";
      PfxSigner signer = new PfxSigner(SignatureAlg.RSA_SHA256, PFX_FILE, PFX_PASS.toCharArray());
      //CERTIFICATE = signer.getSignersCertificate();
      SIGNER = signer;
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  protected void loadPFX(String cnAlias) throws Exception {

    //Pfx okunuyor.
    FileInputStream fis = new FileInputStream("testdata/albania.pfx");
    PfxParser pfxParser = new PfxParser(fis, "123456".toCharArray());

    List<Pair<ECertificate, PrivateKey>> entries = pfxParser.getCertificatesAndKeys();

    for (Pair<ECertificate, PrivateKey> pair : entries) {
      ECertificate cert = pair.getObject1();

      if (cert.getSubject().getCommonNameAttribute().equals(cnAlias)) {
        CERTIFICATE = cert;
        //Subject: C=AL,ST=ST,L=ISTANBUL,O=TUBITAK,OU=EID,CN=UMAY
        System.out.println("CERT is loaded for: " + cert.getSubject().getCommonNameAttribute());

        return;
      }
    }

  }

  /**
   * Creates an appropriate file name for ASiC signatures
   *
   * @param packageType package type of the signature, ASiC_S or ASiC_E
   * @param format format of the signature, CAdES or XAdES
   * @param type type of the signature, BES etc.
   * @return file name of associated signature as string
   */
  protected String fileName(PackageType packageType, SignatureFormat format, SignatureType type) {
    String fileName = BASE_DIR + packageType + "-" + format.name() + "-" + type;
    switch (packageType) {
      case ASiC_S:
        return fileName + ".asics";
      case ASiC_E:
        return fileName + ".asice";
    }
    return null;
  }

  protected String fileName(PackageType packageType, SignatureFormat format, SignatureType type, String name) {
    String fileName = BASE_DIR + packageType + "-" + format.name() + "-" + type + "_" + name;
    switch (packageType) {
      case ASiC_S:
        return fileName + ".asics";
      case ASiC_E:
        return fileName + ".asice";
    }
    return null;
  }

  /**
   * Reads an ASiC signature
   *
   * @param packageType type of the ASiC signature to be read, ASiC_S or ASiC_E
   * @param format format of the ASiC signature to be read, CAdES or XAdES
   * @param type type of the ASiC signature to be read, BES etc.
   * @return signature package of ASiC signature
   */
  protected SignaturePackage read(PackageType packageType, SignatureFormat format, SignatureType type) throws Exception {
    Context c = createContext();
    File f = new File(fileName(packageType, format, type));
    return SignaturePackageFactory.readPackage(c, f);
  }

  /**
   * Creates context for signature creation and validation
   *
   * @return created context
   */
  public Context createContext() {

    System.out.println("Context: ");
    Context c = new Context(new File(BASE_DIR).toURI());
    c.setConfig(new Config(ROOT_DIR + "/config/esya-signature-config.xml"));
    //System.out.println("Context is set: " + c.getConfig().toString());

    //c.setData(getContent()); //for detached CAdES signatures validation
    return c;
  }
}
