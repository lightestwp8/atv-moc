package utils;

import com.objsys.asn1j.runtime.Base64;
import gov.tubitak.eu.http.HttpClient;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;
import java.util.logging.Logger;
import models.Asic;
import org.w3c.dom.*;
import play.Logger.ALogger;
import tr.gov.tubitak.uekae.esya.api.asic.core.impl.ASiCEXAdESPackage;
import tr.gov.tubitak.uekae.esya.api.asic.core.impl.ASiCSXAdESPackage;
import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.asn.x509.ESubjectAltName;
import tr.gov.tubitak.uekae.esya.api.signature.*;
import tr.gov.tubitak.uekae.esya.api.signature.impl.SignableFile;
import tr.gov.tubitak.uekae.esya.api.signature.sigpackage.*;
import tr.gov.tubitak.uekae.esya.asn.x509.IssuerAltName;


import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

public class AsicUtils extends AsicBase {

  private static final ALogger LOGGER = play.Logger.of(AsicUtils.class);

  // signerInfo: The signer alias that is to used to get ASIC data and extract the domain name from
  // the signing certificates issuer alt name field.
  // return value is the domain name and it will be parametered to the DNSClientUtils
  public String getIssuerAltName(String signerInfo) {
    try {
      SignaturePackage signaturePackage = readSignaturePackage(signerInfo);
      return getIssuerAltName(signaturePackage);
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return "";
  }


  // signerInfo: The signer alias that is to used to get ASIC data and extract the domain name from
  // the signing certificates issuer alt name field.
  // return value is the domain name and it will be parametered to the DNSClientUtils
  public String getIssuerAltName(SignaturePackage transaction) {
    try {

      Signature existing = transaction.getContainers().get(0).getSignatures().get(0);

      ECertificate cert = existing.getSignerCertificate();
      String commonName = cert.getIssuer().getCommonNameAttribute();
      String cName = cert.getIssuer().getCountryNameAttribute();
      String oUnit = cert.getIssuer().getOrganizationalUnitNameAttribute();
      IssuerAltName altName = cert.getExtensions().getIssuerAltName();

      if (altName != null) {
        if (altName.elements[0] != null) {
          return altName.elements[0].getElement().toString();
        }
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return "";
  }

  //Extracts the issuer alt name from the certificate
  public String getIssuerAltName(ECertificate cert) {
    try {

      String commonName = cert.getIssuer().getCommonNameAttribute();
      String cName = cert.getIssuer().getCountryNameAttribute();
      String oUnit = cert.getIssuer().getOrganizationalUnitNameAttribute();
      IssuerAltName altName = cert.getExtensions().getIssuerAltName();

      if (altName != null) {
        if (altName.elements[0] != null) {
          return altName.elements[0].getElement().toString();
        }
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return "";
  }

  // Gets the subject alt name from the certificate
  public String getSubjectAltName(ECertificate cert) {
    try {

      ESubjectAltName subjectAltName = cert.getExtensions().getSubjectAltName();

      if (subjectAltName != null) {
        if (subjectAltName.getElementCount() != 0) {

          if (subjectAltName.getElement(0) != null) {
            return subjectAltName.getElement(0).toString();
          }
        }
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return "";
  }

  // Gets the certificate content that will be displayed on the scala.html with name&value
  // only value is displayed on the window
  public ArrayList<Asic> getSignatureCertificateContent(ASiCSXAdESPackage signaturePackage) {
    try {

      ArrayList<Asic> content = new ArrayList<Asic>();

      Signature existing = signaturePackage.getContainers().get(0).getSignatures().get(0);

      ECertificate cert = existing.getSignerCertificate();

      LOGGER.debug(cert.toString());

      String[] str = cert.toString().split("\n");

      for (String each : str) {
        if (each.startsWith("Subject")) {
          content.add(new Asic("Subject: ", each));
        } else if (each.startsWith("Issuer")) {
          content.add(new Asic("Issuer: ", each));
        } else if (each.startsWith("Serial")) {
          content.add(new Asic("Serial No: ", each));
        } else if (each.startsWith("Valid")) {
          content.add(new Asic("Valid From: ", each));
        } else if (each.startsWith("Ca")) {
          content.add(new Asic("CA: ", each));
        }
      }

      //include the issuer Alternative name
      content.add(new Asic("Issuer Alternative Name: ", "Issuer Alternative Name: " + getIssuerAltName(cert)));

      return content;

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }

  // only value is displayed on the window
  public ArrayList<Asic> getCertificateContent(ECertificate cert) {

    try {

      ArrayList<Asic> content = new ArrayList<Asic>();
      String[] str = cert.toString().split("\n");

      for (String each : str) {
        if (each.startsWith("Subject")) {
          content.add(new Asic("Subject: ", each));
        } else if (each.startsWith("Issuer")) {
          content.add(new Asic("Issuer: ", each));
        } else if (each.startsWith("Serial")) {
          content.add(new Asic("Serial No: ", each));
        } else if (each.startsWith("Valid")) {
          content.add(new Asic("Valid From: ", each));
        } else if (each.startsWith("Ca")) {
          content.add(new Asic("CA: ", each));
        }
      }

      //include the issuer Alternative name
      content.add(new Asic("Issuer Alternative Name: ", "Issuer Alternative Name: " + getIssuerAltName(cert)));

      content.add(new Asic("Subject Alternative Name: ", "Subject Alternative Name: " + getSubjectAltName(cert)));

      return content;

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;

  }

  public ArrayList<Asic> getAsicFileContent(ASiCSXAdESPackage signaturePackage) {

    try {

      ArrayList<Asic> content = new ArrayList<>(2);

      PackageValidationResult pvr = signaturePackage.verifyAll();

      Map<SignatureContainerEntry, ContainerValidationResult> map = pvr.getAllResults();

      LOGGER.debug("Manifest File " + signaturePackage.getContents().getMimetype());
      LOGGER.debug(" File Name" + signaturePackage.getContents().getDatas().get(0).getResourceName());
      LOGGER.debug("Signature Name" + signaturePackage.getContents().getContainers().get(0).getASiCDocumentName());

      content.add(new Asic("Manifest File: ", signaturePackage.getContents().getMimetype()));
      content.add(new Asic("File Content: ", signaturePackage.getContents().getDatas().get(0).getResourceName()));
      content.add(
          new Asic("Signature File: ", signaturePackage.getContents().getContainers().get(0).getASiCDocumentName()));

      return content;

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }

  public ASiCSXAdESPackage readSignaturePackage(String shortName) throws SignatureException {

    String fileName = fileName(PackageType.ASiC_S, SignatureFormat.XAdES, SignatureType.ES_BES, shortName);

    LOGGER.debug("FileName 1: " + fileName);

    Context c = createContext();

    ASiCSXAdESPackage signaturePackage = (ASiCSXAdESPackage) SignaturePackageFactory.readPackage(c, new File(fileName));

    return signaturePackage;
  }

  public ASiCSXAdESPackage readSignaturePackage(byte[] transaction) throws SignatureException, IOException {

    Context c = createContext();

    File tmpFile = File.createTempFile("transaction", "dat");
    Files.write(tmpFile.toPath(), transaction);
    ASiCSXAdESPackage signaturePackage = (ASiCSXAdESPackage) SignaturePackageFactory.readPackage(c, tmpFile);

    return signaturePackage;
  }

  // SignerInfo: Signer to sign the electronic transaction, (UMAY, URAZ)
  public ECertificate getSignerCertificate(SignaturePackage signaturePackage) {

    try {
      //validate the ASIC package
      Signature existing = signaturePackage.getContainers().get(0).getSignatures().get(0);

      return existing.getSignerCertificate();

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return null;

    // output results
    //LOGGER.debug(pvr);

  }

  // issuerDomainName: URI returns from the DNS PTR call
  // return String : issuerName
  public String getIssuerName(String issuerDomainName) {

    String[] issuerName = issuerDomainName.split("\\.");

    return issuerName[0];
  }

  public String getDomainName(String issuerDomainName) {

    String[] issuerName = issuerDomainName.split("\\.");

    return issuerName[1] + "." + issuerName[2];
  }

  //Method is used to generate XADES given
  public void createBES(PackageType packageType, SignatureFormat format, String cnName, String fileName, String smime)
      throws Exception {

    Context c = createContext();

    SignaturePackage signaturePackage = SignaturePackageFactory.createPackage(c, packageType, format);

    // add into zip
    Signable inPackage = signaturePackage.addData(new SignableFile(DATA_FILE, smime), fileName);

    SignatureContainer container = signaturePackage.createContainer();

    //update the certificate
    loadPFX(cnName);

    Signature signature = container.createSignature(CERTIFICATE);

    // pass document in ZIP to signature
    signature.addContent(inPackage, false);

    signature.sign(SIGNER);

    String gnfileName = fileName(packageType, format, SignatureType.ES_BES, fileName);
    signaturePackage.write(new FileOutputStream(gnfileName));

    // read it back
    signaturePackage = SignaturePackageFactory.readPackage(c, new File(gnfileName));

    // validate
    PackageValidationResult pvr = signaturePackage.verifyAll();

    // output results
    LOGGER.debug(pvr.toString());
  }

  // This method is developed to download the parameter XML file (trust list) and
  // parse the trust list and returns the certificate of the issuer
  public ECertificate parseTrustList(String fileUrl) {

    try {

      //Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(BASE_DIR + "trust_list.xml"));

      fileUrl = fileUrl.replace("mindertestbed.org", "localhost");

      LOGGER.debug("Trust list URL " + fileUrl);

      byte[] downloadFile = HttpClient.downloadFile(new URL(fileUrl));

      Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
          .parse(new ByteArrayInputStream(downloadFile));

      doc.getDocumentElement().normalize();

      NodeList nl = doc.getElementsByTagName("*");
      Element e;
      Node n;
      NamedNodeMap nnm;

      String attrname;
      String attrval;
      int i, len;

      len = nl.getLength();

      for (int j = 0; j < len; j++) {
        e = (Element) nl.item(j);

        if (e.getTagName().equals("X509Certificate")) {

          Node parentNode = e.getParentNode().getParentNode().getParentNode();

          if (!(parentNode.getNodeName().equals("ServiceDigitalIdentities"))) {

            Node d = e.getFirstChild();

            return new ECertificate(Base64.decode(d.getNodeValue()));
          }

        }

        nnm = e.getAttributes();

        if (nnm != null) {
          for (i = 0; i < nnm.getLength(); i++) {
            n = nnm.item(i);
            attrname = n.getNodeName();
            attrval = n.getNodeValue();
          }
        }
      }


    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return null;
  }

  // Checks the equality for the issuer certificate obtained
  // from the electronic transaction signature --> signer certificate --> issuer name --> from pfx
  // xmlURI is obtained from DNS PTR record that is queried for the issuer certificate obtained from the
  // electronic transaction
  public boolean verifyIssuerCertificate(ECertificate issuerCertificate, SignaturePackage signaturePackage) {

    try {

      //obtain
      ECertificate signerCertificate = getSignerCertificate(signaturePackage);

      if (issuerCertificate != null && signerCertificate != null) {

        signerCertificate.asX509Certificate().verify(issuerCertificate.asX509Certificate().getPublicKey());

        LOGGER.debug("Verification is completed:");

        return true;
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return false;
  }

  public boolean verifyIssuerCertificate(ECertificate issuerCertificate, String signerInfo) throws SignatureException {
    SignaturePackage aSiCSXAdESPackage = readSignaturePackage(signerInfo);
    return verifyIssuerCertificate(issuerCertificate, aSiCSXAdESPackage);
  }
}
