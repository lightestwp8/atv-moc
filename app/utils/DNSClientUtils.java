package utils;

import gov.tubitak.eu.dns.DNSClient;
import org.xbill.DNS.DClass;
import org.xbill.DNS.Type;
import play.Logger;
import play.Logger.ALogger;

public class DNSClientUtils {

  private static final ALogger LOGGER = Logger.of(DNSClientUtils.class);

  private String host = "localhost";
  private int port = 9988;

  // Issues the DNS Query for the domainName
  // returns the XML URI path to be downloaded and parametered to verifyIssuerCerticate in scala Controller
  // to be verified that the issuer certificate from the
  // electtronic transaction is already in the list
  public String issueDNSQuery(String domainName) {
    try {
      LOGGER.debug("DNS Client Api Call " + domainName);

      final DNSClient dnsClient = new DNSClient();

      String query1 = "_scheme._trust." + domainName;

      String[] rrResponse1 = dnsClient.issueDNSQuery(host, port, query1, Type.PTR, DClass.IN);

      String ptrRecord = "";

      if (rrResponse1 != null) {
        ptrRecord = rrResponse1[0];
        LOGGER.debug("Pointer Record " + ptrRecord);
      }

      if (!ptrRecord.isEmpty()) {
        String query2 = "_scheme._trust." + ptrRecord;
        String[] rrResponse2 = dnsClient.issueDNSQuery(host, port, query2, Type.URI, DClass.IN);
        for (String rrName : rrResponse2) {
          LOGGER.debug("Domain Name: " + rrName);

          if (!rrName.startsWith("h")) {
            LOGGER.debug("Domain Name Processed : " + rrName.substring(1));
            return rrName.substring(1);
          } else {
            return rrName;
          }

        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return "";
  }

  // Issues the IN PTR query
  // domainName is the domain name obtained from signer's issuer alt name extension
  public String issueFirstDNSQuery(String domainName) {
    try {
      LOGGER.debug("DNS Client Api Call " + domainName);

      final DNSClient dnsClient = new DNSClient();

      String query1 = "_scheme._trust." + domainName;

      String[] rrResponse1 = dnsClient.issueDNSQuery(host, port, query1, Type.PTR, DClass.IN);

      String ptrRecord = "";

      if (rrResponse1 != null && rrResponse1.length != 0) {
        ptrRecord = rrResponse1[0];
        LOGGER.debug("Pointer Record " + ptrRecord);
        return ptrRecord;
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return "";
  }

  // Issues the DNS Query for the domainName
  // returns the XML URI path to be downloaded and parametered to verifyIssuerCerticate in scala Controller
  // to be verified that the issuer certificate from the
  // electronic transaction is already in the list
  public String issueSecondDNSQuery(String pointerName) {
    try {
      final DNSClient dnsClient = new DNSClient();

      if (!pointerName.isEmpty()) {
        String query2 = "_scheme._trust." + pointerName;

        String[] rrResponse2 = dnsClient.issueDNSQuery(host, port, query2, Type.URI, DClass.IN);
        for (String rrName : rrResponse2) {
          LOGGER.debug("Domain Name: " + rrName);

          if (!rrName.startsWith("h")) {
            LOGGER.debug("Domain Name Processed : " + rrName.substring(1));
            return rrName.substring(1);
          } else {
            return rrName;
          }

        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return "";
  }
}
