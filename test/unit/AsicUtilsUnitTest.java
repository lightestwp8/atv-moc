package unit;

import models.Asic;
import org.junit.Test;
import tr.gov.tubitak.uekae.esya.api.asic.core.impl.ASiCEXAdESPackage;
import tr.gov.tubitak.uekae.esya.api.asic.core.impl.ASiCSXAdESPackage;
import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.signature.*;
import tr.gov.tubitak.uekae.esya.api.signature.impl.SignableFile;
import tr.gov.tubitak.uekae.esya.api.signature.sigpackage.PackageType;
import tr.gov.tubitak.uekae.esya.api.signature.sigpackage.PackageValidationResult;
import tr.gov.tubitak.uekae.esya.api.signature.sigpackage.SignaturePackage;
import tr.gov.tubitak.uekae.esya.api.signature.sigpackage.SignaturePackageFactory;
import utils.AsicBase;
import utils.AsicUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;


public class AsicUtilsUnitTest extends AsicUtils{

  private AsicUtils asicUtils = new AsicUtils();

  @Test
  public void createASiC_S_XAdES_Signer1() throws Exception {
    createBES(PackageType.ASiC_S, SignatureFormat.XAdES, "UMAY", "organic_honey.pdf","application/pdf");
  }
  @Test
  public void createASiC_S_XAdES_Signer2() throws Exception {
    createBES(PackageType.ASiC_S, SignatureFormat.XAdES, "URAZ", "locust.jpg","image/jpeg");
  }

  @Test
  public void createASiC_S_XAdES_XML_List() throws Exception {
    //createBES(PackageType.ASiC_S, SignatureFormat.XAdES, "TLSO");
  }

  @Test
  public void testvalidateAsicContent() throws Exception {

    ASiCSXAdESPackage sigPackage = asicUtils.readSignaturePackage("UMAY");

    System.out.println("Manifest File " + sigPackage.getContents().getMimetype());
    System.out.println(" File Name" + sigPackage.getContents().getDatas().get(0).getResourceName());
    System.out.println("Signature Name" + sigPackage.getContents().getContainers().get(0).getASiCDocumentName());


    System.out.println("Signature Package " + sigPackage);

    ArrayList<Asic> s = (ArrayList) asicUtils.getAsicFileContent(sigPackage);

    System.out.println(s.size());
    }

  @Test
  public void testvalidateAsicSignatureContent() throws Exception {

    ASiCSXAdESPackage sigPackage = asicUtils.readSignaturePackage("UMAY");

    ArrayList<Asic> content = asicUtils.getSignatureCertificateContent(sigPackage);

    System.out.println(content.size());

  }

  @Test
  public void testparseXMLAndValidate() throws Exception {

    ECertificate cert= asicUtils.parseTrustList("http://localhost:9501/trust/a-trust.al.xml");


    if (cert != null) {

      ArrayList<Asic> result = asicUtils.getCertificateContent(cert);

      //boolean result = asicUtils.verifyIssuerCertificate(cert,"UMAY");
      System.out.println("Result " + result.size());
    }

  }

}
